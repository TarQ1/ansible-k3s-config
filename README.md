This is my K3S install playbook using `xanmanning.k3s` and `community.kubernetes`

This is a mono pod cluster

To turn off the cluster edit the line `k3s_state: installed` to `k3s_state: stopped`

To turn uninstall the cluster edit the line `k3s_state: installed` to `k3s_state: uninstalled`

to run
ansible-playbook --ask-become-pass main.yml -i inventory.yml